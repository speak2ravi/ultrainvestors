/**
 * Created by User on 12/8/2015.
 */
Schema = {};

Schema.login = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: "E-mail",
        optional: true
    },
    password: {
        type: String,
        label: "Password"
    }
});

ultraInvestors = new Mongo.Collection("ultraInvestors");

/**Schema Define for storeUsers Form**/
Schema.ultraInvestors = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: "E-mail",
        optional: true
    },
    password: {
        type: String,
        label: "Password *",
        min: 6,
    },
    investorID:{
        type:String,
        optional: true
    }
});

ultraInvestors.attachSchema(Schema.ultraInvestors, {transform: true});

portfolios = new Mongo.Collection("portfolios");

/**Schema Define for storeUsers Form**/
Schema.portfolios = new SimpleSchema({
    investorID: {
        type: String
    },
    balance: {
        type: Number,
        decimal: true,
    }
});

portfolios.attachSchema(Schema.portfolios, {transform: true});

Schema.createUltraInvestors = new SimpleSchema({
    email: {
        type: String,
        regEx: SimpleSchema.RegEx.Email,
        label: "E-mail",
        optional: true
    },
    password: {
        type: String,
        label: "Password *",
        min: 6,
    },
    investorID:{
        type:String,
        optional: true
    },
    secretKey:{
        type:String
    }
});