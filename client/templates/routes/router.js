/**
 * Created by User on 12/8/2015.
 */

Router.configure({
    loadingTemplate: "loading"
});


Router.map(function () {
    this.route("login", {path: "/", controller: "LoginController"});
    this.route("logout", {path: "/logout", onBeforeAction:function () {
    	Session.set('investorID','');
        Session.set('balance','');

        this.redirect("/");
    }});
    this.route("createUltraUser", {path: "/create", controller: "CreateUltraUserController"})
    this.route("portfolio",{path: "/portfolio", controller: "PortfolioController"})
});

