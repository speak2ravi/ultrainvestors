Template.Login.events({
    'submit form#loginForm': function (event) {
        event.preventDefault();

        $('#processing').addClass('processing');
        var password =  CryptoJS.MD5($('.password').val()).toString();

        var postContent = {
            'email': $('.email').val(),
            'password': password
        };

        Meteor.call('loginUltraUser', {postContent: postContent}, function(err, response){
            if(err) {
                console.log("Error in Calling loginUltraUser API " + err.reason);
                Session.set('serverDataResponse', "Error:" + err.reason);
                return;
            }else{
                if(response==='sorry'){
                    alert("Please login with valid email or password.");
                    $('#processing').removeClass('processing');
                }else{
                    Session.set('investorID',response.investorID);
                    Session.set('balance', response.balance);
                    Router.go('portfolio');
                }
            }
        });
    }
});
