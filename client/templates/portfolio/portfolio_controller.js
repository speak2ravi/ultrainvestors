/**
 * Created by User on 12/8/2015.
 */
this.PortfolioController = RouteController.extend({
    template: "portfolio",


    yieldTemplates: {
        /*YIELD_TEMPLATES*/
    },

    onBeforeAction: function() {
        this.next();
    },

    action: function() {
        if(Session.get('investorID'))
        {
            this.render();
        }
        else{
            this.render("Login");
        }
    },

    subscriptionSetup: function() {
        var subs = [
        ];
        var ready = true;
        _.each(subs, function(sub) {
            if(!sub.ready())
                ready = false;
        });
        return ready;
    },

    data: function() {


        return {
            params: this.params || {}
        };
        /*DATA_FUNCTION*/
    },

    onAfterAction: function() {

    }
});
