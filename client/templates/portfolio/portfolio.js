/**
 * Created by User on 12/8/2015.
 */
Template.portfolio.helpers({

    balance: function () {
        var balance = Session.get('balance');
        return balance;
    },

    disclaimer: function () {
        return "This is an unaudited result and we accept no responsibility to any parties for the accuracy of this figure.";
    },

});