/**
 * Created by User on 12/8/2015.
 */
/************************************************
 *
 *    Create Order Section
 *	   # OnCreated
 *      # OnRendered
 *      # Helpers
 *      # Events
 *
 ************************************************/
/*******************OnCreated*******************/
Template.createUltraUser.onCreated(function() {

});

/*******************OnRendered*******************/
Template.createUltraUser.rendered = function () {

}

Template.createUltraUser.helpers({



});


/*******************Events*******************/
Template.createUltraUser.events({
    'submit form#insertUltraInvestorsForm': function(event) {
        event.preventDefault();

        $('#processing').addClass('processing');

        var email = $('#email').val();
        var password =$('#password').val();
        var investorID = $('#investorID').val();
        var secretKey = $('#secretKey').val();

        console.log(email);
        console.log(password);
        console.log(investorID);


        var postContent = {
            email :  email,
            password :  CryptoJS.MD5(password).toString(),
            investorID :investorID,
            secretKey:secretKey
        };


        Meteor.call('createUltraUser', {postContent: postContent}, function(err, response){
            if(err) {
                console.log("Error in Calling createUltraUser API " + err.reason);
                Session.set('serverDataResponse', "Error:" + err.reason);
                return;
            }else{
                if(response==='Exists'){
                    alert("user with Email ID already exist.");
                    $('#processing').removeClass('processing');
                }else if(response==='Success'){
                    alert("User created sucessfully");
                }
                else if(response==='Not Authorized'){
                    alert("You dont have entitlements to create users.")
                }
            }
        });
    },

});






