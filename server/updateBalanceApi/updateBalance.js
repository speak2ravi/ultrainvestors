if (Meteor.isServer) {

    var request = Meteor.npmRequire('request');


    Router.map(function(){
        this.route('updateInvestorBalance', {
            path: '/api/v1/updateInvestorBalance',
            where: 'server',
            action: function(){
               var data = this.request.query;

                console.log(data.investorID);
                console.log(data.secretKey);
                console.log(data.balance);
                if(data.investorID && data.balance && data.secretKey)
                {

                    var secretKey = data.secretKey;

                    if(secretKey === "artlugnidart12158855125849"){

                        var investorID = data.investorID;
                        var balance = data.balance;

                        var isExisting = portfolios.findOne({investorID:investorID});

                        if(isExisting){
                           var count = portfolios.update({investorID:investorID},{$set: {balance:balance}},{multi:false});

                            if(count === 1){
                                var rtrn = '{"status" : "Success", "Message":"Investor "'+investorID +'" balance is updated."}';
                                return this.response.end(rtrn);
                            }
                        }
                        else{
                            var rtrn = '{"status" : "Fail", "Message":"Investor "'+investorID +'" does not exist."}';
                            return this.response.end(rtrn);
                        }
                    }
                    else{
                        var rtrn = '{"status" : "Fail", "Message":"Not Authorized"}';
                        return this.response.end(rtrn);
                    }
                }
                else{
                    var rtrn = '{"status" : "Fail", "Message":"Missing parameters"}';
                    return this.response.end(rtrn);
                }
            }

        });
    });
}


