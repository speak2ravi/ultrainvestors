/**
 * Created by User on 12/8/2015.
 */
if (Meteor.isServer) {
    var fs = Npm.require('fs');
    var path = Npm.require('path');

    Meteor.methods({
        loginUltraUser: function (doc) {

            var email = doc.postContent.email;
            var password= doc.postContent.password;
            console.log(email);
            console.log(password);

            var ultraInvestor = ultraInvestors.find(
                {
                    email: email,
                    password: password }
            ).fetch();

            if(ultraInvestor.length===1){

                var investorID = ultraInvestor[0].investorID.toString();

                var portfolio = portfolios.find({investorID: investorID }).fetch();

                if(portfolio.length>0){
                    console.log("found portfolio");
                    var investorInfo = {
                        investorID: portfolio[0].investorID,
                        balance: portfolio[0].balance
                    };
                }
                else{
                    console.log("create dummy portfolio");
                    // first time login, create a 0 balance account
                    portfolios.insert({investorID:ultraInvestor[0].investorID,balance:0});

                    var investorInfo = {
                        investorID: ultraInvestor[0].investorID,
                        balance: 0
                    };
                }
                return investorInfo;
            } else {
                console.log("no records found");
                return 'sorry';
            }
        }
    });
}
